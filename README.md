# RunPayroll-method-in-Java



public class RunPayroll{
 
  public static void main(String [] args){
   
   // Create a CompanyPayroll objects
   CompanyPayroll cP = new CompanyPayroll();
   
   // Calculate the payroll
   cP.calculatePayroll();
   
   // Display the result
   System.out.println("Total gross payroll is" + cP.getPayroll());
  
  }
 }  
  
